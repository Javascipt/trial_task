var sprintf = require('sprintf')

module.exports = {
    createDate : function (dateStr) {
        return new Date(sprintf('%sT23:00:00.000Z', dateStr));
    },
    createTime : function (timeStr) {
        return new Date(sprintf('1970-01-01T%s:00.000Z', timeStr));
    }
}