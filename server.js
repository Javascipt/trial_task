var express = require('express');
var app = express();

app.use('/', express.static('public'));
app.use('/lib', express.static('bower_components'));

app.listen(8000, function () {
    console.log("Listening at port 8000");
});