### Installing dependencies

```
> bower install
> npm install
```

### launching server

```
> npm start
```

The server is going to listen at port 8000. You can then browse the url : http://localhost:8000

### Running tests
You need first to install Mocha globally to be able to run tests:

```
> npm install -g mocha
```

Then, you can run tests:

```
> npm test
```
