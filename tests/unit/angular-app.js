var Browser = require('zombie');
var assert = require('assert');
var util = require("../../modules/util");
var url = "http://localhost:8000";

describe('Unit testing Angular app', function () {
    describe('Testing Angular App', function () {
        var browser, angular;
        beforeEach(function (done) {
            browser = new Browser({ site: url });
            browser.visit('/').then(function () {
                angular = browser.window.angular;
                done();
            });
        });
        
        it('Should find the angular app', function () {
            assert.equal(angular.module('myApp').name , 'myApp');
        });
        
        it('Should find the booking controller', function () {
            assert.equal(typeof angular.element('input').scope() , 'object');
        });
    });
    describe('Testing Angular form validation', function () {
        var browser, $scope;
        beforeEach(function (done) {
            browser = new Browser({ site: url });
            browser.visit('/').then(function () {
                $scope = browser.window.angular.element('input').scope();
                done();
            });
        });
        
        it('Should refuse one-way empty data', function () {
            assert.equal($scope.booking_request_form.$valid, false);
        });
        
        it('Should refuse one-way partially emtpy data', function () {
            $scope.data = {
                reqPickupInput: 'Tangier',
                reqDropoffInput: 'Berlin'
            }
            $scope.$apply();
            assert.equal($scope.booking_request_form.$valid, false);
        });
        
        it('Should accept correct one-way data', function () {
            $scope.data = {
                reqPickupInput: 'Tangier',
                reqDropoffInput: 'Berlin',
                reqAtDateInput: util.createDate('2016-02-01'),
                reqAtTimeInput: util.createTime('00:00')
            }
            $scope.$apply();
            assert.equal($scope.booking_request_form.$valid, true);
        });
        
        it('Should refuse two-way empty data', function () {
            $scope.config.isReturnRide = true;
            assert.equal($scope.booking_request_form.$valid, false);
        });
        
        it('Should refuse one-way partially emtpy data', function () {
            $scope.config.isReturnRide = true;
            $scope.data = {
                reqPickupInput: 'Tangier',
                reqDropoffInput: 'Berlin',
                reqAtDateInput: util.createDate('2016-02-01'),
                reqAtTimeInput: util.createTime('00:00'),
                reqReturnPickupInput: 'Berlin',
                reqReturnDropoffInput: 'Tangier'
            }
            $scope.$apply();
            assert.equal($scope.booking_request_form.$valid, false);
        });
        
        it('Should accept correct two-way data', function () {
            $scope.config.isReturnRide = true;
            $scope.data = {
                reqPickupInput: 'Tangier',
                reqDropoffInput: 'Berlin',
                reqAtDateInput: util.createDate('2016-02-01'),
                reqAtTimeInput: util.createTime('00:00'),
                reqReturnPickupInput: 'Berlin',
                reqReturnDropoffInput: 'Tangier',
                reqReturnAtDateInput: util.createDate('2017-02-01'),
                reqReturnAtTimeInput: util.createTime('00:00')
            }
            $scope.$apply();
            assert.equal($scope.booking_request_form.$valid, true);
        });
    });
    
    describe('Testing Angular form submission one-way ride', function () {
        var browser, $scope;
        beforeEach(function (done) {
            browser = new Browser({ site: url });
            browser.visit('/').then(function () {
                $scope = browser.window.angular.element('input').scope();
                $scope.data = {
                    reqPickupInput: 'Tangier',
                    reqDropoffInput: 'Berlin',
                    reqAtDateInput: util.createDate('2016-02-01'),
                    reqAtTimeInput: util.createTime('00:00')
                }
                $scope.$apply();
                browser.document.getElementsByTagName('form')[0].submit();
                done();
            });
        });
        
        it('should send form data to blacklane booking request url', function() {
            assert.equal(browser.document.location.href, 'https://www.blacklane.com/en/booking_requests/transfers/new');
        });
    });
    
    describe('Testing Angular form submission two-way ride', function () {
        var browser, $scope;
        beforeEach(function (done) {
            browser = new Browser({ site: url });
            browser.visit('/').then(function () {
                $scope = browser.window.angular.element('input').scope();
                $scope.data = {
                    reqPickupInput: 'Tangier',
                    reqDropoffInput: 'Berlin',
                    reqAtDateInput: util.createDate('2016-02-01'),
                    reqAtTimeInput: util.createTime('00:00'),
                    reqReturnPickupInput: 'Berlin',
                    reqReturnDropoffInput: 'Tangier',
                    reqReturnAtDateInput: util.createDate('2017-02-01'),
                    reqReturnAtTimeInput: util.createTime('00:00')
                }
                $scope.$apply();
                browser.document.getElementsByTagName('form')[0].submit();
                done();
            });
        });
        
        it('should send form data to blacklane booking request url', function() {
            assert.equal(browser.document.location.href, 'https://www.blacklane.com/en/booking_requests/transfers/new');
        });
    });
});