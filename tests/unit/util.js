var util = require('../../modules/util');
var assert = require('assert');

describe('Unit testing util module', function () {
    describe('Testing .createDate() method', function () {
        
        var date1 = util.createDate('2016-01-aa')
            , date2 = util.createDate('2016/01/02')
            , date3 = util.createDate('')
            , date4 = util.createDate('Invalid')
            , date5 = util.createDate('2015\\02\\29')
            , date6 = util.createDate('2016-02-85')
            , date7 = util.createDate('02-01-2016')
            , date8 = util.createDate('2016-02-01');
        
        it('Should create Invalid Date when input is not valid', function () {
            assert.equal(date1.toJSON(), null);
            assert.equal(date2.toJSON(), null);
            assert.equal(date3.toJSON(), null);
            assert.equal(date4.toJSON(), null);
            assert.equal(date5.toJSON(), null);
            assert.equal(date6.toJSON(), null);
            assert.equal(date7.toJSON(), null);
        });
        
        it('Should create Date when input is valid', function () {
            assert.equal(date8.toJSON(), '2016-02-01T23:00:00.000Z');
        });
    });
    
    describe('Testing .createTime() method', function () {
        
        var time1 = util.createTime('00-00')
            , time2 = util.createTime('00/00')
            , time3 = util.createTime('')
            , time4 = util.createTime('Invalid')
            , time5 = util.createTime('00\\00')
            , time6 = util.createTime('52:25')
            , time7 = util.createTime('26:51:22')
            , time8 = util.createTime('22:00');
        
        it('Should create Invalid Date when input is not valid', function () {
            assert.equal(time1.toJSON(), null);
            assert.equal(time2.toJSON(), null);
            assert.equal(time3.toJSON(), null);
            assert.equal(time4.toJSON(), null);
            assert.equal(time5.toJSON(), null);
            assert.equal(time6.toJSON(), null);
            assert.equal(time7.toJSON(), null);
        });
        
        it('Should create Date when input is valid', function () {
            assert.equal(time8.toJSON(), '1970-01-01T22:00:00.000Z');
        });
    });
});