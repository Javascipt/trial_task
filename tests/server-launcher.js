var express = require('express');

var port = 8000;
var url = 'http://localhost:' + port

// Launching Express server
var app = express();
app.use('/', express.static('public'));
app.use('/lib', express.static('bower_components'));
app.listen(port);