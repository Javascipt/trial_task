var Browser = require('zombie');
var assert = require('assert');
var url = "http://localhost:8000";

describe('Functional and integration testing' , function () {
    describe('Testing Http Server', function () {
        var browser;
        beforeEach(function (done) {
            browser = new Browser({ site: url });
            // from loading, serving and browsing, default 2s isn't sufficient and test may fail
            // That's why I used this.timeout 4s
            this.timeout(4000);
            browser.visit('/').then(done);
        });
        
        it('Should show the booking page', function () {
            assert.equal(browser.success, true);
            assert.equal(browser.text('title'), 'BOOK A RIDE');
        });
    });
    
    describe('Testing empty submission', function() {
        var browser;
        beforeEach(function (done) {
            browser = new Browser({ site: url });
            browser.visit('/').then(done);
        });
        
        it('should refuse empty submission', function() {
            assert.equal(browser.button('BOOK A RIDE').disabled, true);
        });
        
        it('should refuse partial submission', function() {
            browser.fill('booking_request[pickup]', 'Tangier');
            browser.fill('booking_request[dropoff]', 'Berlin');
            assert.equal(browser.button('BOOK A RIDE').disabled, true);
        });
    });
    
    describe('Testing invalid data', function() {
        var browser;
        beforeEach(function (done) {
            browser = new Browser({ site: url });
            browser.visit('/').then(done);
        });
        
        it('should refuse invalid date', function() {
            browser.fill('booking_request[pickup]', 'Tangier')
                .fill('booking_request[dropoff]', 'Berlin')
                .fill('booking_request[at_date]', '123')
                .fill('booking_request[at_time]', '00:00');
            assert.equal(browser.button('BOOK A RIDE').disabled, true);
        });
        
        it('should refuse invalid time', function() {
            browser.fill('booking_request[pickup]', 'Tangier')
                .fill('booking_request[dropoff]', 'Berlin')
                .fill('booking_request[at_date]', '2017-01-01')
                .fill('booking_request[at_time]', '123');
            assert.equal(browser.button('BOOK A RIDE').disabled, true);
        });
    });
    
    describe('Testing two-way ride inputs', function() {
        var browser;
        beforeEach(function (done) {
            browser = new Browser({ site: url });
            browser.visit('/').then(done);
        });
        
        it('should not find two-way ride inputs', function() {
            assert.equal(browser.document.getElementsByTagName('input').length, 6);
        });
        
        it('should find two-way ride inputs', function() {
            browser.check('[type=checkbox]');
            assert.equal(browser.document.getElementsByTagName('input').length, 10);
        });
    });
    
    describe('Testing empty two-way ride submission', function() {
        var browser;
        beforeEach(function (done) {
            browser = new Browser({ site: url });
            browser.visit('/').then(done);
        });
        
        it('should refuse fully empty submission', function() {
            browser.check('[type=checkbox]');
            assert.equal(browser.button('BOOK A RIDE').disabled, true);
        });
        
        it('should refuse empty submission', function() {
            browser.fill('booking_request[pickup]', 'Tangier')
                .fill('booking_request[dropoff]', 'Berlin')
                .fill('booking_request[at_date]', '2017-01-01')
                .fill('booking_request[at_time]', '00:00')
                .check('[type=checkbox]');
            assert.equal(browser.button('BOOK A RIDE').disabled, true);
        });
        
        it('should refuse partially empty submission', function() {
            browser.fill('booking_request[pickup]', 'Tangier')
                .fill('booking_request[dropoff]', 'Berlin')
                .fill('booking_request[at_date]', '2017-01-01')
                .fill('booking_request[at_time]', '00:00')
                .check('[type=checkbox]')
                .fill('booking_request[return_pickup]', 'Berlin')
                .fill('booking_request[return_dropoff]', 'Tangier');
            assert.equal(browser.button('BOOK A RIDE').disabled, true);
        });
    });
    
    describe('Testing invalid two-way ride data', function() {
        var browser;
        beforeEach(function (done) {
            browser = new Browser({ site: url });
            browser.visit('/').then(function () {
                done();
            });
        });
        
        it('should refuse invalid date', function() {
            browser.fill('booking_request[pickup]', 'Tangier')
                .fill('booking_request[dropoff]', 'Berlin')
                .fill('booking_request[at_date]', '2017-01-01')
                .fill('booking_request[at_time]', '00:00')
                .check('[type=checkbox]')
                .fill('booking_request[return_pickup]', 'Tangier')
                .fill('booking_request[return_dropoff]', 'Berlin')
                .fill('booking_request[return_at_date]', '123')
                .fill('booking_request[return_at_time]', '00:00');
            assert.equal(browser.button('BOOK A RIDE').disabled, true);
        });
        
        it('should refuse invalid time', function() {
            browser.fill('booking_request[pickup]', 'Tangier')
                .fill('booking_request[dropoff]', 'Berlin')
                .fill('booking_request[at_date]', '2017-01-01')
                .fill('booking_request[at_time]', '00:00')
                .check('[type=checkbox]')
                .fill('booking_request[return_pickup]', 'Tangier')
                .fill('booking_request[return_dropoff]', 'Berlin')
                .fill('booking_request[return_at_date]', '2017-01-01')
                .fill('booking_request[return_at_time]', '123');
            assert.equal(browser.button('BOOK A RIDE').disabled, true);
        });
    });
    
    describe('Testing valid data for one-way and two-way', function() {
        var browser;
        beforeEach(function (done) {
            browser = new Browser({ site: url });
            browser.visit('/').then(function () {
                done();
            });
        });
        
        it('should accept valid one-way date', function() {
            browser.fill('booking_request[pickup]', 'Tangier')
                .fill('booking_request[dropoff]', 'Berlin')
                .fill('booking_request[at_date]', '2017-01-01')
                .fill('booking_request[at_time]', '00:00')
            assert.equal(browser.button('BOOK A RIDE').disabled, false);
        });
        
        it('should accept valid two-wat data', function() {
            browser.fill('booking_request[pickup]', 'Tangier')
                .fill('booking_request[dropoff]', 'Berlin')
                .fill('booking_request[at_date]', '2017-01-01')
                .fill('booking_request[at_time]', '00:00')
                .check('[type=checkbox]')
                .fill('booking_request[return_pickup]', 'Tangier')
                .fill('booking_request[return_dropoff]', 'Berlin')
                .fill('booking_request[return_at_date]', '2017-01-02')
                .fill('booking_request[return_at_time]', '00:00');
            assert.equal(browser.button('BOOK A RIDE').disabled, false);
        });
    });
    
    describe('Testing form submission for one-way ride', function() {
        var browser;
        beforeEach(function (done) {
            browser = new Browser({ site: url });
            browser.visit('/').then(function () {
                browser.fill('booking_request[pickup]', 'Tangier')
                    .fill('booking_request[dropoff]', 'Berlin')
                    .fill('booking_request[at_date]', '2017-01-01')
                    .fill('booking_request[at_time]', '00:00')
                    .pressButton('BOOK A RIDE');
                done();
            });
        });
        
        it('should send form data to blacklane booking request url', function() {
            assert.equal(browser.document.location.href, 'https://www.blacklane.com/en/booking_requests/transfers/new');
        });
    });
    
    describe('Testing form submission for two-way ride', function() {
        var browser;
        beforeEach(function (done) {
            browser = new Browser({ site: url });
            browser.visit('/').then(function () {
                browser.fill('booking_request[pickup]', 'Tangier')
                    .fill('booking_request[dropoff]', 'Berlin')
                    .fill('booking_request[at_date]', '2017-01-01')
                    .fill('booking_request[at_time]', '00:00')
                    .check('[type=checkbox]')
                    .fill('booking_request[return_pickup]', 'Tangier')
                    .fill('booking_request[return_dropoff]', 'Berlin')
                    .fill('booking_request[return_at_date]', '2017-01-02')
                    .fill('booking_request[return_at_time]', '00:00')
                    .pressButton('BOOK A RIDE');
                done();
            });
        });
        
        it('should send form data to blacklane booking request url', function() {
            assert.equal(browser.document.location.href, 'https://www.blacklane.com/en/booking_requests/transfers/new');
        });
    });
});