var myApp = angular.module('myApp', []);

myApp.controller('bookingController', function ($scope) {
    $scope.data = {};
    $scope.config = { isReturnRide : false };
    
    $scope.addresses = [
        { "name": "Aéroport de Berlin-Tegel (TXL)" },
        { "name": "Aéroport de Berlin-Schönefeld (SXF)" },
        { "name": "Aéroport de Bergame-Orio al Serio (BGY)" },
        { "name": "Berjaya Eden Park" },
        { "name": "Hotel Ibis Styles Berlin Alexanderplatz" }
    ];
})
